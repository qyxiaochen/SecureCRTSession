#SecureCRT 批量制作Session使用说明
#因主机变多，制作session时间太长且烦
#SecureCRT  Version 7.3.5 (build 903)   -   Official Release - September 10, 2015
#WINDOWS环境执行，支持SSH连接方式模版
#by qyxiaochen@163.com
1、目录文件说明
 D:\SecureCRT\VanDyke\Config\Sessions\makesession----程序目录
 D:\SecureCRT\VanDyke\Config\Sessions\session   ----程序输出session路径，可修改
 D:\SecureCRT\VanDyke\Config\Sessions\vbsscript ----程序输出自动登录脚本路径，可修改
 admin.xlsx 								 ---主机信息excl
 Private_session_temp.ini    ---直连主机模版，文件名勿改
 Pubilc_session_temp.ini		----支持跳转主机模板，文件名勿改
 script.vbs									----执行主程序和配置文件
 script_temp.vbs            ----自动登录脚本，文件名勿改

2、模版生成方法
2.1 仅需直连主机session文件，模版生成方法，即局域网内登录session
    2.1.1通过SecureCRT新建连接：File=>Connect=>点击 New Session按钮，出现以下窗口，填写连接的名字，
    协议（SSH1，SSH2，Telnet, Rlogin等）
    2.1.2点击SSH2 选项，填写主机名、IP 地址、端口号、用户名等。另外可设置会话窗口的颜色方案，点击  Appearance 选项，
    可自己设计或者选择已有的颜色方案，更改字体，光标等。
    2.1.3登录会话并登录服务器，提示输入密码选项等。
    2.1.4在SecureCRT安装目录 D:\SecureCRT\VanDyke\Config\Sessions下找到新增的对应session文件，
    将文件复制到程序目录下，重命名为Private_session_temp.ini
2.2 仅需支持跳转主机session文件，模版生成方法，即通过公网堡垒机登录跳转局域网内主机；
    2.2.1 参考2.1步骤新增session，IP地址使用跳转主机地址或堡垒机地址。
          将文件复制到程序目录下，重命名为Pubilc_session_temp.ini
    2.2.2 连接新增的session，建立登录会话并登录服务器，
    开始录制脚本：Script -> Start Recording Script
　　输入登录的命令：ssh 用户名@ip地址 -p 端口    ;输入密码：Password:
		多次跳转重复命令，结束录制：Script -> Stop Recording Script
　　把脚本存成script_temp.vbs，设置登录脚本，选中会话，右键Properties -> Connection -> Logon Scripts:
　　在“Logon scrip”前打钩，选中script_temp.vbs。
		2.2.3 将script_temp.vbs文件复制到程序目录下。
3、使用说明
   1、脚本执行前需要修改对应参数项，在script.vbs中，使用文本编辑打开，修改如下参数项：
 sConfigFilePath = "D:\SecureCRT\VanDyke\Config\Sessions\makesession\" 	'指定配置文件夹路径,如更换路径要保持格式一致；
 sScriptFilePath = "D:\SecureCRT\VanDyke\Config\Sessions\vbsscript\" '指定自动登录脚本路径,如更换路径要保持格式一致；
 sCnfOutFilePath = "D:\SecureCRT\VanDyke\Config\Sessions\session\" '指定输出session路径,如更换路径要保持格式一致；
 sSession_temp = "Pubilc_session_temp.ini"   '指定session模版，每次仅生成一种类型session，类型Pubilc_session_temp或Private_session_temp,模版参考readme生成
 sScript_temp = "script_temp.vbs"    '指定vbs自动登录模板，仅在公网且需要跳转的session生效
 sExcelFileName	= "admin.xlsx" '指定excel文件，文件格式不变
 sDefaultHost = "192.168.3.9" '设置默认模版中被替换地址
 sDefaultPass = "Scxxxxx"	 '设置默认模板中被替换密码
   2、脚本执行，打开SecureCRT程序： Script -> Run，选中script.vbs，执行即可。