﻿#$language = "VBScript"
#$interface = "1.0"
'Function  path_click(Files_path,Templates)
' If len(Templates)=0 or len(Files_path)=0 Then '判断输入是否为空
'    msgbox "输入不能为空! ",vbinformatin, "提示 "     
' else   		
'    pathvule = 1
' end if
'end Function 
Sub Main()
sConfigFilePath =InputBox("指定配置文件夹路径,读取excel文件及sessin模版文件路径","请保持路径格式一致","D:\session制作脚本\makesession")
sScriptFilePath =InputBox("指定vbs登录模板路径","请保持路径格式一致","D:\session制作脚本\makesession")
sCnfOutFilePath =InputBox("指定输出session路径","请保持路径格式一致","D:\session制作脚本\test")
sSession_temp =InputBox("指定session模版名称","模版请参考readme说明生成","Pubilc_session_temp.ini or Private_session_temp.ini")
sScript_temp =InputBox("指定vbs登录模板","仅在公网且需要跳转的session生效","script_temp.vbs")
sExcelFileName =InputBox("指定excel文件","请保持文件格式不变","admin.xlsx")
sDefaultHost =InputBox("设置默认vbs登录模板中被替换地址","请使用IP地址格式","10.10.10.9")
sDefaultPass =InputBox("设置默认vbs登录模板中被替换密码","ScXXXXX"	 )
SecureCRTversion=InputBox("设置CRT大版本号，取主版本号即可，7.x 取7；6.x 取6","7"	 )
if len(sConfigFilePath)<>0 and len(sScriptFilePath)<>0 and len(sCnfOutFilePath)<>0 and len(sSession_temp)<>0 and len(sScript_temp)<>0 and len(sExcelFileName)<>0 and vbOK = ( MsgBox ("请确认已修改配置参数，确定执行，否则取消", vbOKCancel, "提示" )) then
 Set oExcel = CreateObject("Excel.Application")	'创建Excel应用对象 
 Set oWorkbooks = oExcel.Workbooks.Open(sConfigFilePath&"\"&sExcelFileName)	'打开Excel文件
 sActiveSheetName = oWorkbooks.ActiveSheet.Name	'获取当前活动工作表名
 Set oSheet = oWorkbooks.Sheets(sActiveSheetName)	'创建工作表对象
 Set oFSO = CreateObject("Scripting.FileSystemObject")	'创建文件系统对象
 iRow = 2		'设置Excel单元格行号
 iCol = 2		'设置Excel单元格列号
 Do until oSheet.Cells(iRow,iCol).Value=""		'直到第1列单元格为空为止
 	sHost	= oSheet.Cells(iRow,iCol+1).Value			'获取设备host 
 	sAddr = oSheet.Cells(iRow,iCol+2).Value			'获取设备ipaddr
 	sUser	= oSheet.Cells(iRow,iCol+3).Value			'获取设备host
 	sPass = oSheet.Cells(iRow,iCol+4).Value			'获取密码Passwd
 	sOutputFileName = sCnfOutFilePath&"\"&sHost&"_"&sAddr&".ini"	'输出session路径和配置文件名称
 	sOutputScriptName = sScriptFilePath&"\"&sHost&"_"&sAddr&".vbs"	'输出脚本vbsScript路径和文件名称
 	sHostname ="S:"&chr(34)&"Hostname"&chr(34)&"="	'Hostname配置项字串
 	sUsername ="S:"&chr(34)&"Username"&chr(34)&"="	'User配置项字串
 	if SecureCRTversion > 6 then
 		sScriptname		="S:"&chr(34)&"Script Filename V2"&chr(34)&"="	'User配置项字串
	else
 		sScriptname		="S:"&chr(34)&"Script Filename"&chr(34)&"="	'User配置项字串
 	end if
 	sInputFileName = sConfigFilePath&"\"&sSession_temp	'指定配置文件模板名
 	Set oOutputFile= oFSO.CreateTextFile(sOutputFileName)	'打开指定配置文件
 	Set oInputFile = oFSO.OpenTextFile(sInputFileName) '打开配置文件模板
 	Do Until oInputFile.AtEndofStream '直到配置文件模板的文件尾
 		sLine = oInputFile.ReadLine '读入一行
 		If sSession_temp = "Private_session_temp.ini" and InStr(sLine,sHostname) = 1 then '匹配私网模版+主机名参数
 			  oOutputFile.WriteLine sHostname&sAddr '匹配参数项设置=IP配置值
 		ElseIf sSession_temp = "Private_session_temp.ini" and InStr(sLine,sUsername) = 1 then '匹配私网模板+用户名参数
 			  oOutputFile.WriteLine sUsername&sUser '匹配参数项设置=Username配置值
 		ElseIf sSession_temp = "Pubilc_session_temp.ini" and InStr(sLine,sScriptname) = 1 then '匹配公网模版+用户名参数
 			  oOutputFile.WriteLine sScriptname&sOutputScriptName '匹配参数项设置=sScript配置值
 			 Else
 				oOutputFile.WriteLine sLine    '无参数项则保持模版
 		End If 
 	Loop
 	oInputFile.Close    '关闭配置文件模板
 	oOutputFile.Close		'关闭配置文件
 	If 	sSession_temp = "Pubilc_session_temp.ini" then  '匹配公网模版
 			sInputScriptName = sConfigFilePath&"\"&sScript_temp	'指定配置脚本模板名
 			Set oInputScript = oFSO.OpenTextFile(sInputScriptName) '打开脚本文件模板
 	    Set oOutputScript = oFSO.CreateTextFile(sOutputScriptName)	'打开指定脚本文件  
      Do Until oInputScript.AtEndofStream 				'直到脚本文件模板的文件尾
 		  sLine = oInputScript.ReadLine 							'读入一行
 		  If InStr(sLine,sDefaultHost) > 0  Then   		'找模版中主机IP字符串项
 			sLine = Replace(sLine,sDefaultHost,sAddr) '替换脚本主机IP字符串项
 			oOutputScript.WriteLine sLine							'写入替换后行
 		  Elseif  InStr(sLine,sDefaultPass)> 0 Then   '找模版中密码字符串项
 	    sLine = Replace(sLine,sDefaultPass,sPass) '替换脚本密码字符串项
 	    oOutputScript.WriteLine sLine							'写入替换后行
 		  Else 
 			oOutputScript.WriteLine sLine             '无参数项则保持模版
 		  End If 
 	    Loop
 	    oInputScript.Close    '关闭脚本文件模板
 	    oOutputScript.Close		'关闭脚本文件
 	End If
 iRow = iRow+1       '指向表1的下一个记录
 Loop
 oExcel.Application.DisplayAlerts=False  '关闭Excel应用对象
 oExcel.Quit 
 Set oFSO=nothing     '关闭文件系统对象
MsgBox "  执行完成，请验证生成文件 ", vbInformation,"提示" 
Else 
MsgBox "未执行，退出结束", vbInformation,"提示"
End If
End Sub
