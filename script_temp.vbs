#$language = "VBScript"
#$interface = "1.0"

crt.Screen.Synchronous = True

' This automatically generated script may need to be
' edited in order to work correctly.

Sub Main
	crt.Screen.Send "ssh user1@xxx.xxx.xxx.xxx -p 22" & chr(13)
	crt.Screen.WaitForString "xxx@xxx.xxx.xxx.xxx's password: "
	crt.Screen.Send "Scxxxxxx" & chr(13)
	crt.Screen.WaitForString "[user2@HOSTNAME " & chr(126) & "]$ "
	crt.Screen.Send "ssh user3@bbb.bbb.bbb.bbb -p 22" & chr(13)
	crt.Screen.WaitForString "user3@bbb.bbb.bbb.bbb's password: "
	crt.Screen.Send "Scxxxxxx" & chr(13)
End Sub